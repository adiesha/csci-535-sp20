\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage{algorithm}
\usepackage{algpseudocode}
\algnewcommand\algorithmicforeach{\textbf{for each}}
\algdef{S}[FOR]{ForEach}[1]{\algorithmicforeach\ #1 \algorithmicdo}

\usepackage{amsfonts}
\usepackage{mathtools}

\def\R{{\mathbb R}}
\def\N{{\mathbb N}}

\title{Quiz 02 (Group)}
\author{Computational Topology}
\date{4 February 2020}

\begin{document}
\maketitle

In today's class, we will explore two concepts important for analyzing
algorithms: recurrence relations and loop invariants. Please see the handwritten
notes posted on Slack as a supplement to this worksheet.

Please work in your assigned groups.  I suggest spending the first 30 minutes on
Part 1 (Recurrence Relations) and the last 45 minutes on Part 2 (Loop
Invariants).  You have until the end of the day on Wednesday to hand in the
worksheet.

\noindent
Group members (must be present today):


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{Recurrence Relations}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{By Hand}
When analyzing the runtime of an algorithm, we usually go line-by-line, then sum
the runtime of the lines.  However, what happens when an algorithm calls itself?
(e.g., in binary search, we test the middle point then recurse on a set half of
the size).  To analyze these algorithms, we create a variable that is a function
of $n$ to explain what the runtime is on a problem of size $n$.  So, the
recusion for binary search is:
$$T(n)=T(n/2)+\Theta(1).$$
If you know binary search, then we can expand the recursion tree (see
handwritten handout) in order to try to see that the closed form of this
recurrence is $\Theta( \log n)$.
Try this yourselves.
What are the closed forms of the following recurrences?  Use recursion trees to
find the answers.
\begin{enumerate}
    \item $T(n) + T(n/4) + T(3n/4) + \Theta(n)$
    \item $T(n) + 2T(n/2)+ \Theta(n^2)$
\end{enumerate}

Another option is to guess-and-check, then to use induction to prove your
answer.  We won't do that in class today though.

\subsection{Master's Theorem}
Master's theorem allows us to quickly solve recurrence relations of the form:
$$ T(n) = a T(n/b) + f(n),$$
where $a, b \in \N$ such that $a \geq 1$ and $b >0$ and $f(n)$ is asymptotically
positive.  Then, we can determine the closed-form of $T(n)$ as follows:
\begin{enumerate}
    \item IF there exists $\varepsilon \in \R_+$ such that $f(n) \in O(n^{\log_b
        a - \varepsilon})$, THEN $T(n) \in \Theta(n^{\log_b a})$.
    \item IF there exists $\varepsilon \in \R_+$ such that $f(n) \in \Theta(n^{\log_b
        a})$, THEN $T(n) \in \Theta(n^{\log_b a}\log n)$.
    \item IF (1) there exists $\varepsilon \in \R_+$ such that $f(n) \in O(n^{\log_b
        a + \varepsilon})$
        and (2) there exists $c \in (0,1)$ and $n_0 \in \N$ such that for all $n
        \geq n_0$, $a f(n/b) \leq c f(n)$, THEN $T(n) \in \Theta(f(n))$.
\end{enumerate}

\begin{table}[h!]
    \centering
    \begin{tabular}{|l|l|l|l|l|l|l|l|l|}
        \hline
        &  $a$ & $b$  & $\log_b a$  & $n^{\log_b a}$  & $f(n)$  & Potential
        Case? & $\varepsilon$, if Case 1 or 3  & Closed Form \\ \hline
        \hline
        $T(n) = T(n/2)+1$             & &  &  &  &  & & & \\[5ex] \hline
        $T(n) = 2 T(n/4) + \sqrt{n}$  & &  &  &  & &  & & \\[5ex] \hline
        $T(n) = 2 T(n/4) + n$         & &  &  &  &  & & &  \\[5ex] \hline
        $T(n) = 2 T(n/4) + n^2$       & &  &  &  &  & & & \\[5ex] \hline
        $T(n) = 3 T(n/3) + \Theta(1)$ & &  &  &  & & & & \\[5ex] \hline
    \end{tabular}
\end{table}

Remember, Case 3 has an additional condition to check! Do that in the space
provided below, or on a separate sheet of paper.\\
\fbox{\begin{minipage}{6.5in}\hfill\vspace{3in}\end{minipage}}

\vspace{1in}
Give an example of a recurrence relation that cannot be solved using
recursion.\\
\fbox{\begin{minipage}{6.5in}\hfill\vspace{3in}\end{minipage}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{Loop Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

See the handwritten notes on loop invariants.
First, decide what the precondion, postcondition, loop
guard, and loop
invariant are.  Second, please prove the three properties of the loop
invariant.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALGORITHM
\begin{algorithm}\caption{\textsc{Binary-Search}}
 \begin{algorithmic}[1]
     \State {\bf Input:} sorted Array $A$ (indexed $1$ through $A.length$, query value $q$
   \State {\bf Output:} boolean
   \State $start \gets 1$
   \State $end \gets A.length$
   \While{$start \leq end$}
   \State $mid = \lfloor{(start+end)/2}\rfloor$
   \If{$q=A[mid]$}
      \State {\bf return} true
   \ElsIf{$q < mid$}
      \State $start \gets mid +1 $
   \Else
      \State $end \gets mid -1$
   \EndIf
   \EndWhile\\
   \Return False
 \end{algorithmic}
\end{algorithm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALGORITHM

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALGORITHM
  \begin{algorithm}\caption{\textsc{BFS}}
 \begin{algorithmic}[1]
     \State {\bf Input:} a graph $G=(V,E)$; a starting vertex $s$
 \State {\bf Output:} each vertex now has a value $dist$ that is it's graph
     distance to $s$.

 \ForEach{vertex $u \in V$}
 \State $u.marked \gets FALSE $
 \State $u.d \gets \infty$
 \EndFor

 \State $s.dist \gets 0$
 \State $Q \gets \emptyset$
 \State \textsc{Enqueue}$(Q,s)$

 \While{$Q \neq \emptyset$}
 \State $u \gets \textsc{Dequeue} (Q)$
 \State $u.marked \gets TRUE$
 \ForEach {$v \in u.neighbors$}
 \If {$v.marked == FALSE$}
 \State $v.dist \gets u.dist+1$
 \State \textsc{Enqueue}$(Q,v)$
 \EndIf
 \EndFor
 \EndWhile

 \end{algorithmic}
\end{algorithm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALGORITHM

\end{document}
